/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 97.91044776119404, "KoPercent": 2.08955223880597};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7095588235294118, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-23"], "isController": false}, {"data": [0.95, 500, 1500, "https://demowebshop.tricentis.com/logout-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-21"], "isController": false}, {"data": [0.95, 500, 1500, "https://demowebshop.tricentis.com/logout-20"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-18"], "isController": false}, {"data": [0.4, 500, 1500, "https://demowebshop.tricentis.com/login-19"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/login"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/logout"], "isController": false}, {"data": [0.95, 500, 1500, "https://demowebshop.tricentis.com/login-25"], "isController": false}, {"data": [0.95, 500, 1500, "https://demowebshop.tricentis.com/login-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-24"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [0.95, 500, 1500, "https://demowebshop.tricentis.com/login-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-22"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-24"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-20"], "isController": false}, {"data": [0.725, 500, 1500, "https://demowebshop.tricentis.com/login-2"], "isController": false}, {"data": [0.575, 500, 1500, "https://demowebshop.tricentis.com/login-1"], "isController": false}, {"data": [0.575, 500, 1500, "https://demowebshop.tricentis.com/login-4"], "isController": false}, {"data": [0.625, 500, 1500, "https://demowebshop.tricentis.com/login-3"], "isController": false}, {"data": [0.625, 500, 1500, "https://demowebshop.tricentis.com/login-6"], "isController": false}, {"data": [0.625, 500, 1500, "https://demowebshop.tricentis.com/login-5"], "isController": false}, {"data": [0.65, 500, 1500, "https://demowebshop.tricentis.com/login-8"], "isController": false}, {"data": [0.725, 500, 1500, "https://demowebshop.tricentis.com/login-7"], "isController": false}, {"data": [0.65, 500, 1500, "https://demowebshop.tricentis.com/login-0"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/logout-5"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/logout-4"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-3"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-2"], "isController": false}, {"data": [0.55, 500, 1500, "https://demowebshop.tricentis.com/logout-1"], "isController": false}, {"data": [0.85, 500, 1500, "https://demowebshop.tricentis.com/logout-0"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/logout-9"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/logout-8"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-7"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-6"], "isController": false}, {"data": [0.675, 500, 1500, "https://demowebshop.tricentis.com/login-9"], "isController": false}, {"data": [0.55, 500, 1500, "https://demowebshop.tricentis.com/logout-12"], "isController": false}, {"data": [0.85, 500, 1500, "https://demowebshop.tricentis.com/logout-11"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/logout-10"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-16"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-17"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-19"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/login-14"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-18"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/login-15"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-17"], "isController": false}, {"data": [0.725, 500, 1500, "https://demowebshop.tricentis.com/login-12"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-16"], "isController": false}, {"data": [0.55, 500, 1500, "https://demowebshop.tricentis.com/login-13"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/logout-15"], "isController": false}, {"data": [0.675, 500, 1500, "https://demowebshop.tricentis.com/login-10"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-14"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/login-11"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/logout-13"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 670, 14, 2.08955223880597, 612.8507462686567, 1, 5325, 339.0, 1173.8, 2345.4999999999964, 3622.8899999999985, 28.615358332621508, 988.0962311544374, 51.81687171670795], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://demowebshop.tricentis.com/logout-23", 10, 0, 0.0, 175.1, 159, 264, 164.5, 255.40000000000003, 264.0, 264.0, 0.9289363678588017, 0.11248838829540175, 0.8400342545285647], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-22", 10, 0, 0.0, 202.0, 163, 514, 168.5, 479.8000000000001, 514.0, 514.0, 0.9181050312155711, 0.11117678112376056, 0.8212736412045538], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-21", 10, 0, 0.0, 166.4, 161, 178, 165.5, 177.2, 178.0, 178.0, 0.9173470323823503, 0.11018914549123934, 0.8250748211173287], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-20", 10, 0, 0.0, 216.90000000000003, 160, 674, 165.5, 624.2000000000002, 674.0, 674.0, 0.9165062780680048, 0.11187820777197324, 0.8126833012556136], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-18", 10, 0, 0.0, 584.3, 345, 1397, 503.0, 1312.0000000000005, 1397.0, 1397.0, 0.7535795026375283, 41.02813559721176, 0.8433614355689526], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-19", 10, 0, 0.0, 1130.4, 865, 1618, 1043.5, 1606.7, 1618.0, 1618.0, 0.7015574575557738, 301.96225072786586, 0.7851414515223797], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login", 20, 1, 5.0, 3443.8999999999996, 2470, 5325, 3342.5, 5139.500000000003, 5321.7, 5325.0, 0.9770873027505008, 545.3870887836484, 19.229822383848745], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout", 10, 4, 40.0, 2779.7, 1967, 4926, 2706.5, 4743.500000000001, 4926.0, 4926.0, 0.8069071249899137, 32.60826739389171, 17.187515759904784], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-25", 10, 0, 0.0, 221.79999999999998, 161, 677, 168.5, 628.6000000000001, 677.0, 677.0, 0.7785736530675802, 2.9964441081438804, 0.8873002471971349], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-23", 10, 0, 0.0, 225.9, 164, 674, 172.5, 627.0000000000002, 674.0, 674.0, 0.7843752451172641, 3.0294961859753706, 0.9000399541140481], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-24", 10, 0, 0.0, 191.2, 164, 359, 173.0, 341.30000000000007, 359.0, 359.0, 0.7758553805570642, 2.9852248040965166, 0.8978404550391808], "isController": false}, {"data": ["Test", 10, 4, 40.0, 9667.5, 8263, 11843, 9581.0, 11747.5, 11843.0, 11843.0, 0.8042464211034261, 930.3235144060641, 48.78720582173878], "isController": true}, {"data": ["https://demowebshop.tricentis.com/login-21", 10, 0, 0.0, 213.8, 163, 588, 170.5, 548.8000000000002, 588.0, 588.0, 0.7745333436604446, 5.760591743474556, 0.8819393346758578], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-22", 10, 0, 0.0, 167.9, 163, 179, 165.0, 178.6, 179.0, 179.0, 0.78125, 3.006744384765625, 0.901031494140625], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-24", 10, 1, 10.0, 158.9, 84, 190, 164.5, 188.0, 190.0, 190.0, 0.9307520476545049, 0.3932063826321668, 0.7436018068224125], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-20", 10, 0, 0.0, 170.60000000000002, 162, 200, 165.5, 197.8, 200.0, 200.0, 0.7716644802839726, 2.310471969287754, 0.8831941122000154], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-2", 20, 0, 0.0, 584.5, 163, 2323, 499.5, 1032.3000000000002, 2258.999999999999, 2323.0, 1.194243745148385, 14.963034424075953, 1.1680030378575268], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-1", 20, 0, 0.0, 809.4000000000001, 358, 1583, 768.0, 1170.2, 1562.4499999999998, 1583.0, 1.2062726176115801, 76.76323130277443, 1.1049645657418579], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-4", 20, 0, 0.0, 916.05, 165, 2509, 781.5, 2396.2000000000016, 2507.35, 2509.0, 1.1924636298592892, 55.07050907017649, 1.1744136432745051], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-3", 20, 0, 0.0, 762.75, 167, 2373, 817.0, 2052.4000000000024, 2362.8999999999996, 2373.0, 1.1929615269907545, 15.97333544586937, 1.1836415150611392], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-6", 20, 0, 0.0, 622.95, 168, 1988, 672.0, 835.1, 1930.4499999999991, 1988.0, 1.1940298507462688, 4.888059701492537, 1.1380597014925373], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-5", 20, 0, 0.0, 742.35, 167, 2318, 749.0, 2059.1000000000026, 2311.0, 2318.0, 1.1933174224343677, 14.292423366646776, 1.1239791542362767], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-8", 20, 1, 5.0, 586.5, 1, 2898, 660.5, 1228.7000000000012, 2817.249999999999, 2898.0, 1.244864932154861, 4.785131936231794, 1.1337510114527574], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-7", 20, 0, 0.0, 676.4999999999999, 163, 3063, 468.5, 1431.2, 2981.549999999999, 3063.0, 1.2276717205819165, 137.01248005033455, 1.191704775642993], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-0", 20, 0, 0.0, 844.25, 217, 1607, 939.0, 1470.9, 1600.25, 1607.0, 1.1680196227296618, 13.686589309700402, 0.7824818956958476], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-5", 10, 0, 0.0, 491.80000000000007, 167, 775, 666.5, 769.9, 775.0, 775.0, 0.9304922303898763, 0.11267679352377408, 0.8314456941472039], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-4", 10, 0, 0.0, 486.0, 162, 1012, 586.5, 978.8000000000002, 1012.0, 1012.0, 0.9305788200260562, 0.11177851060859854, 0.8360669086171598], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-3", 10, 0, 0.0, 407.5, 160, 738, 338.0, 731.3000000000001, 738.0, 738.0, 0.9296272194849865, 0.11257204610951009, 0.8134238170493633], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-2", 10, 0, 0.0, 385.9, 164, 675, 253.0, 674.8, 675.0, 675.0, 0.9294544102611767, 0.11255111999256436, 0.8096419276884469], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-1", 10, 0, 0.0, 636.0, 398, 1019, 635.0, 987.7, 1019.0, 1019.0, 0.9111617312072894, 31.397707858769934, 0.6842611047835991], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-0", 10, 0, 0.0, 363.40000000000003, 198, 746, 217.0, 743.5, 746.0, 746.0, 0.9487666034155597, 0.6402321513282733, 0.9913869781783682], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-9", 10, 1, 10.0, 512.4, 163, 686, 661.0, 685.3, 686.0, 686.0, 0.888888888888889, 0.3802083333333333, 0.69921875], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-8", 10, 1, 10.0, 636.1999999999999, 166, 1177, 655.5, 1160.8000000000002, 1177.0, 1177.0, 0.9106638739641199, 0.3839195257717876, 0.707543142701029], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-7", 10, 0, 0.0, 430.29999999999995, 162, 1635, 173.0, 1538.4000000000003, 1635.0, 1635.0, 0.8738967054094207, 0.10497001441929564, 0.7518583959625973], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-6", 10, 0, 0.0, 430.7, 163, 954, 334.0, 926.8000000000001, 954.0, 954.0, 0.929195316855603, 0.11251974540048318, 0.8402684212971566], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-9", 20, 0, 0.0, 511.80000000000007, 165, 726, 664.0, 721.5, 725.8, 726.0, 1.2075836251660428, 2.086147883709697, 1.1722051986475062], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-12", 10, 1, 10.0, 630.5, 164, 1328, 668.5, 1263.6000000000004, 1328.0, 1328.0, 0.8893632159373889, 0.37493955109391675, 0.6863035285485592], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-11", 10, 0, 0.0, 348.5, 160, 675, 169.0, 674.8, 675.0, 675.0, 0.9321401938851603, 0.11287635160328113, 0.8092506175428784], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-10", 10, 0, 0.0, 572.0, 169, 692, 667.5, 690.8, 692.0, 692.0, 0.8891259891526629, 0.10766760024895528, 0.7745120921134525], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-16", 10, 0, 0.0, 165.39999999999998, 159, 170, 165.0, 170.0, 170.0, 170.0, 0.7663422484481569, 0.09130249444401871, 0.92724418537819], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-17", 10, 0, 0.0, 174.7, 162, 214, 166.5, 212.20000000000002, 214.0, 214.0, 0.7688167909587145, 1.4850777465979856, 0.8709252710079188], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-19", 10, 0, 0.0, 166.80000000000004, 159, 185, 164.0, 183.70000000000002, 185.0, 185.0, 0.9295408068414204, 0.11256158207845325, 0.8287800357873212], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-14", 10, 0, 0.0, 586.5, 324, 1333, 332.0, 1290.5000000000002, 1333.0, 1333.0, 0.7641754546843955, 22.257356382775484, 0.8723838930918538], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-18", 10, 1, 10.0, 201.7, 161, 521, 167.5, 486.2000000000001, 521.0, 521.0, 0.8893632159373889, 0.43903623599252933, 0.694120197438634], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-15", 10, 0, 0.0, 300.7, 162, 965, 165.5, 940.2, 965.0, 965.0, 0.7663422484481569, 0.09205087554601886, 0.9040443712161852], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-17", 10, 0, 0.0, 561.0, 161, 3090, 169.5, 2849.100000000001, 3090.0, 3090.0, 0.888888888888889, 0.1076388888888889, 0.7699652777777778], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-12", 20, 0, 0.0, 488.09999999999997, 165, 1247, 652.5, 750.8000000000001, 1222.2999999999997, 1247.0, 1.2183978068839476, 4.515448903441974, 1.1838924002436797], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-16", 10, 1, 10.0, 284.90000000000003, 160, 663, 166.0, 661.1, 663.0, 663.0, 0.9296272194849865, 0.39681646253602304, 0.7312643801710514], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-13", 10, 0, 0.0, 702.1, 167, 1670, 671.5, 1600.8000000000002, 1670.0, 1670.0, 0.7668123610152595, 0.09285618434169159, 0.9165804002760525], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-15", 10, 1, 10.0, 342.0, 162, 677, 175.5, 676.1, 677.0, 677.0, 0.9320533134495294, 0.39867124149501354, 0.7077779849007364], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-10", 20, 0, 0.0, 504.4500000000001, 165, 706, 660.5, 704.1, 705.95, 706.0, 1.20962864400629, 2.3590121114067983, 1.1735996658400871], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-14", 10, 1, 10.0, 206.2, 163, 564, 167.5, 524.9000000000001, 564.0, 564.0, 0.9314456035767511, 0.39268072955476896, 0.7449745598919523], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-11", 20, 0, 0.0, 515.45, 166, 1270, 651.5, 877.4000000000003, 1251.2499999999998, 1270.0, 1.2160272390101539, 0.923895695263574, 1.1691199382866178], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-13", 10, 0, 0.0, 415.0, 160, 675, 411.5, 674.7, 675.0, 675.0, 0.9005763688760806, 0.1090541696685879, 0.7739328170028819], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Test failed: message expected to contain /OK/", 5, 35.714285714285715, 0.746268656716418], "isController": false}, {"data": ["Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 9, 64.28571428571429, 1.3432835820895523], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 670, 14, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 9, "Test failed: message expected to contain /OK/", 5, "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login", 20, 1, "Test failed: message expected to contain /OK/", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout", 10, 4, "Test failed: message expected to contain /OK/", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-24", 10, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-8", 20, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-9", 10, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-8", 10, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-12", 10, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-18", 10, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-16", 10, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-15", 10, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-14", 10, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
